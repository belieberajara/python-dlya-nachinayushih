'''
Задания к уроку 2. Переменные
'''

# 1. Отредактируйте код, что бы он выводил заданный текст
apple_stocks = 355
apple_stocks = apple_stocks + 1
print("Запасы яблок на складе", apple_stocks, "кг")
# требуемый вывод:
# Запасы яблок на складе: 356 кг.

# 2. Допишите код, что бы получить требуемый вывод
a, b = 45, 54
c = a + 1
d = c + 10
print(d)
# требуемый вывод:
# 56

# 3. Создайте переменные, что бы вывести требуемый текст
programming_lang_1 = python
programming_lang_2 = javascript
programming_lang_3 = php
print("Мой стек:", programming_lang_1, programming_lang_2, programming_lang_3)
# требуемый вывод:
# Мой стек: python, javascript, php
